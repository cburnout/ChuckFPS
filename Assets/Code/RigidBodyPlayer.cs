using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* [RequireComponent(typeof(RigidBody))] */
public class RigidBodyPlayer : MonoBehaviour {
  public float speed = 100f;
  // Start is called before the first frame update
  public Rigidbody rb;
  public GameObject groundCheck;
  public float groundCheckMagnitude;
  public float jumpForce;
  public LayerMask groundMask;
  void Start() { rb = GetComponent<Rigidbody>(); }
  void Awake() { groundMask = LayerMask.GetMask("Ground"); }

  // Update is called once per frame
  void Update() {
    getInput();
    jumpCheck();
  }

  void getInput() {
    Vector2 xMove =
        new Vector2(Input.GetAxisRaw("Horizontal") * transform.right.x,
                    Input.GetAxisRaw("Horizontal") * transform.right.z);
    Vector2 zMove =
        new Vector2(Input.GetAxisRaw("Vertical") * transform.forward.x,
                    Input.GetAxisRaw("Vertical") * transform.forward.z);
    /* Vector2 yMove; */

    Vector2 vel = (xMove + zMove).normalized * speed * Time.deltaTime;
    rb.velocity = new Vector3(vel.x, rb.velocity.y, vel.y);
  }
  void jumpCheck() {
    bool isGrounded = Physics
                          .OverlapSphere(groundCheck.transform.position,
                                         groundCheckMagnitude, groundMask)
                          .Length > 0;
    Debug.Log(isGrounded);
    if (isGrounded & Input.GetKeyDown(KeyCode.Space)) {
      rb.AddForce(new Vector3(0, jumpForce, 0));
    }
  }
}
