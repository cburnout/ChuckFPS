using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MapGenerator : MonoBehaviour {
  private int sizeOfGround = 10;

  public int desiredMacroSize;
  public int desiredMicroSize;
  public int desiredMicroBlockPadding;

  public GameObject ground;
  public GameObject groundStart;
  public GameObject groundFinish;

  List<GameObject> gameObjectsToBake;

  public bool useMacroSeed;
  public int macroSeed;
  public int macroMinDepth;
  public int macroMaxDepth;

  public bool useMicroSeed;
  public int microSeed;
  public int microMinDepth;
  public int microMaxDepth;

  public Vector3Int start;
  public Vector3Int finish;
  public MapGeneratorAbstract pathOfLength;
  public MapGeneratorAbstract pathBetweenPoints;
  private Hashtable macroBlockEdges;
  // Start is called before the first frame update
  public void Start() {
    /* Random.InitState(42179); */
    if (desiredMacroSize % desiredMicroSize != 0) {
      Debug.LogWarning("desiredMicroSize must evenly divide desiredMacroSize");
    }
    if (desiredMacroSize % sizeOfGround != 0) {
      Debug.LogWarning("sizeOfGround must evenly divide desiredMacroSize");
    }

    macroBlockEdges = new Hashtable();
    generateMacroBlockEdges();

    if (useMacroSeed) {
      Random.InitState(macroSeed);
    } else {
      Random.InitState(System.Environment.TickCount);
    }

    pathOfLength =
        new PathOfLength(start, finish, macroMinDepth, macroMaxDepth);
    Vector3[] points = pathOfLength.generatePath();
    int scale = desiredMacroSize / sizeOfGround;
    List<GameObject> gameObjectsToBake =
        placeTiles(points, sizeOfGround, scale);
    bakeNavMesh(gameObjectsToBake);
  }

  List<GameObject> placeTiles(Vector3[] points, int size, int scale) {
    if (useMicroSeed) {
      Random.InitState(microSeed);
    } else {
      Random.InitState(System.Environment.TickCount);
    }
    List<GameObject> gameObjectsToBake = new List<GameObject>();
    Vector3 lift = new Vector3(0f, 1f, 0f);
    Vector3 startBlock = Vector3.zero;
    Vector3 finishBlock = Vector3.zero;
    for (int i = 0; i < points.Length; i++) {
      bool startOfBlocksFlag = i == 0;
      bool endOfBlocksFlag = i == points.Length - 1;
      Vector3 prevP = Vector3.zero;
      Vector3 p = points[i];
      Vector3 nextP = Vector3.zero;

      GameObject bigTileTemp = placeBigTiles(i, points.Length, p, size, scale);
      gameObjectsToBake.Add(bigTileTemp);

      if (i - 1 < 0) {
        nextP = points[i + 1];
        Vector3 delta = nextP - p;
        prevP = p - delta;
      } else if (i < points.Length - 1) {
        prevP = points[i - 1];
        nextP = points[i + 1];
      } else if (i - 1 >= 0) {
        prevP = points[i - 1];
        Vector3 delta = p - prevP;
        nextP = p + delta;
      }

      Vector3 prevDir = prevP - p;
      /* Vector3 nextDir = p - nextP; */
      Vector3 nextDir = nextP - p;
      if (startOfBlocksFlag) {
        startBlock = generateStartOfBlock(prevP, p, prevDir);
      }
      /* Instantiate(groundStart, p * 100 + startBlock * sizeOfGround + lift, */
      /*             Quaternion.identity); */
      finishBlock = generateFinishOfBlock(nextP, p, nextDir);
      if (endOfBlocksFlag) {
        finishBlock = finishBlock - nextDir;
      }
      /* Instantiate(groundFinish, p * 100 + finishBlock * sizeOfGround + lift, */
      /*             Quaternion.identity); */

      PathBetweenPoints pathBetweenPoints = new PathBetweenPoints(
          startBlock, finishBlock, microMinDepth, microMaxDepth);
      pathBetweenPoints.setMinX(-1);
      pathBetweenPoints.setMinZ(-1);
      pathBetweenPoints.setMaxX(10);
      pathBetweenPoints.setMaxZ(10);
      Vector3[] microPath = pathBetweenPoints.generatePath();
      Debug.Log(microPath.Length);
      placeMicroBlock(microPath, p, size, scale);
      startBlock = new Vector3(Mod(finishBlock.x + nextDir.x, scale),
                               Mod(finishBlock.y + nextDir.y, scale),
                               Mod(finishBlock.z + nextDir.z, scale));
    }
    return gameObjectsToBake;
  }

  public List<GameObject> placeMicroBlock(Vector3[] points,
                                          Vector3 vectorToMacroBlock, int size,
                                          int scale) {
    List<GameObject> microBlockGameObjects = new List<GameObject>();
    Vector3 lift = new Vector3(0, 2, 0);
    Vector3 offset = vectorToMacroBlock * size * scale;
    for (int i = 0; i < points.Length; i++) {
      Vector3 p = points[i];
      GameObject temp = Instantiate(ground, offset + lift + p * sizeOfGround,
                                    Quaternion.identity);
    }

    return microBlockGameObjects;
  }

  public GameObject placeBigTiles(int i, int len, Vector3 p, int size,
                                  float scale) {
    /* This is where we put down the big squares */
    GameObject temp =
        Instantiate(ground, p * size * scale, Quaternion.identity);
    temp.transform.localScale = Vector3.one * scale;
    temp.transform.parent = transform;
    temp.name = p.x.ToString() + ", " + p.y.ToString() + ", " + p.z.ToString();
    var tempRenderer = temp.GetComponentInChildren<Renderer>();
    float cStep = 0.75f / len;
    float c = cStep * i + 0.125f;
    var newColor = new Vector4(0, 0, c, 1);
    tempRenderer.material.SetColor("_BaseColor", newColor);
    return temp;
  }

  public Vector3 generateFinishOfBlock(Vector3 nextP, Vector3 p,
                                       Vector3 nextDir) {
    List<Vector3> offsets = (List<Vector3>) macroBlockEdges[nextDir];
    int r = Random.Range(0, offsets.Count);
    Vector3 finishOffset = offsets[r];
    Vector3 scaledUp = p * 100;
    return offsets[r];
  }
  public Vector3 generateStartOfBlock(Vector3 prevP, Vector3 p,
                                      Vector3 prevDir) {
    List<Vector3> offsets = (List<Vector3>) macroBlockEdges[prevDir];
    int r = Random.Range(0, offsets.Count);
    Vector3 scaledUp = p * 100;
    return offsets[r];
  }

  private void printMacroBlockEdgeFromKey(Vector3 key) {
    List<Vector3> l = (List<Vector3>) macroBlockEdges[key];
    string l_str = "";
    foreach (Vector3 v in l) {
      l_str += v.ToString() + ": ";
    }
    Debug.Log(string.Format("{0}: {1}", key, l_str));
  }

  void bakeNavMesh(List<GameObject> gameObjectsToBake) {
    /* Debug.Log(gameObjectsToBake.Count); */
    foreach (var navMesh in gameObjectsToBake) {
      navMesh.GetComponentInChildren<NavMeshSurface>().BuildNavMesh();
    }
  }

  float Mod(float a, int n) { return (a % n + n) % n; }

  void generateMacroBlockEdges() {
    /*
     * This all sucks. It creates the hashtable so we can figure out offsets
     * Really there is a closed form for this but I couldn't get it to work
     * so i gave up and did this.
     */
    int microBlocks = desiredMacroSize / desiredMicroSize;
    int x;
    int z;
    x = 0;
    z = -1;
    List<Vector3> currentList;
    currentList = new List<Vector3>();
    Vector3 key;
    key = new Vector3(x, 0, z);
    for (int i = desiredMicroBlockPadding;
         i < microBlocks - desiredMicroBlockPadding; i++) {
      currentList.Add(new Vector3(x + i, 0, 0));
    }
    macroBlockEdges.Add(key, currentList);

    x = 0;
    z = 1;
    currentList = new List<Vector3>();
    key = new Vector3(x, 0, z);
    for (int i = desiredMicroBlockPadding;
         i < microBlocks - desiredMicroBlockPadding; i++) {
      currentList.Add(new Vector3(x + i, 0, microBlocks - 1));
    }
    macroBlockEdges.Add(key, currentList);

    x = -1;
    z = 0;
    currentList = new List<Vector3>();
    key = new Vector3(x, 0, z);
    for (int i = desiredMicroBlockPadding;
         i < microBlocks - desiredMicroBlockPadding; i++) {
      currentList.Add(new Vector3(0, 0, z + i));
    }
    macroBlockEdges.Add(key, currentList);

    x = 1;
    z = 0;
    currentList = new List<Vector3>();
    key = new Vector3(x, 0, z);
    for (int i = desiredMicroBlockPadding;
         i < microBlocks - desiredMicroBlockPadding; i++) {
      currentList.Add(new Vector3(microBlocks - 1, 0, z + i));
    }
    macroBlockEdges.Add(key, currentList);
  }
}
