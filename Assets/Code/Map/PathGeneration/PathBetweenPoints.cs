using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathBetweenPoints : MapGeneratorAbstract
{
    private Vector3 start;
    private Vector3 finish;
    private int maxDepth;
    private int minDepth;
    private int minX;
    private int minZ;
    private int maxX;
    private int maxZ;
    private double maxExplored = 1000;
    private List<Vector3> visited;

    public PathBetweenPoints(Vector3 start, Vector3 finish, int minDepth, int maxDepth) {
        this.start = start;
        this.finish = finish;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }

    override public void setStart(Vector3 start){
        this.start = start;
    }
    override public void setFinish(Vector3 finish){
        this.finish = finish;
    }
    override public void setMaxDepth(int maxDepth){
        this.maxDepth = maxDepth;
    }
    override public void setMinDepth(int minDepth){
        this.minDepth = minDepth;
    }
    public void setMinX(int x) {
        this.minX = x;
    }
    public void setMinZ(int x) {
        this.minZ = x;
    }
    public void setMaxX(int x) {
        this.maxX = x;
    }
    public void setMaxZ(int x) {
        this.maxZ = x;
    }
    override public Vector3[] generatePath(){
        Stack<Vector3> path = new Stack<Vector3>();
        List<Vector3> visited = new List<Vector3>();
        path.Push(start);
        visited.Add(start);

        path = restartCutout(path);
        return path.ToArray();
    }

    private Stack<Vector3> restartCutout(Stack<Vector3> path) {

        int explored = 0;
        while (path.Peek() != finish) {
            explored = 0;
            path = dfsSearchPoint(path, ref explored);
        }
        return path;
    }
    private Stack<Vector3> dfsSearchPoint(Stack<Vector3> path, ref int explored){
        explored++;
        if (explored > maxExplored) {
            return path;
        }
        if (path.Peek() == finish && path.Count > minDepth) {
            return path;
        }
        if (path.Count > maxDepth) {
            return path;
        }
        Vector3 current = path.Peek();
        ArrayList options = getAdjacentV3s(current, path);
        while (options.Count > 0) {
            int r = Random.Range(0, options.Count);
            Vector3 chosen = (Vector3) options[r];
            path.Push(chosen);
            options.RemoveAt(r);
            path = dfsSearchPoint(path, ref explored);
            if (path.Peek() == finish && path.Count > minDepth) {
                break;
            }
            path.Pop();
        }
        return path;
    }

    private ArrayList getAdjacentV3s(Vector3 v, Stack<Vector3>visited) {
        ArrayList adj = new ArrayList();
        Vector3 nv = new Vector3(v.x-1, v.y, v.z);
        if(!visited.Contains(nv) & nv.x > minX){
            adj.Add(nv);
        }
        nv = new Vector3(v.x+1, v.y, v.z);
        if(!visited.Contains(nv) & nv.x < maxX){
            adj.Add(nv);
        }
        nv = new Vector3(v.x, v.y, v.z-1);
        if(!visited.Contains(nv) & nv.z > minZ){
            adj.Add(nv);
        }
        nv = new Vector3(v.x, v.y, v.z+1);
        if(!visited.Contains(nv) & nv.z < maxZ){
            adj.Add(nv);
        }
        return adj;
    }
}
